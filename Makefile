all: clean build compress

clean:
	rm -rf dist/
	mkdir dist

build:
	API_HOST=https://api.app.tareas.nulo.in pnpm run build

compress:
	gzip -fk dist/*.css dist/*.js dist/*.html dist/img/icons/*.svg
	brotli -fk dist/*.css dist/*.js dist/*.html dist/img/icons/*.svg

# deploy:
# 	rsync -e 'ssh -p 420' -ruhP dist/ hnrd@192.168.86.123:/srv/hnrd/frontend/
