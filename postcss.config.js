const prod = process.env.NODE_ENV === 'production'
module.exports = {
  plugins: [
    require('tailwindcss'),
    require('postcss-nested'),
    require('autoprefixer'),
    ...(prod ? [require('cssnano')] : []),
  ],
}
