#!/bin/sh
convert logo.svg.png -resize 144x144 msapplication-icon-144x144.png
convert logo.svg.png -resize 150x150 mstile-150x150.png
convert logo.svg.png -resize 16x16 favicon-16x16.png
convert logo.svg.png -resize 32x32 favicon-32x32.png
convert logo.svg.png -resize 192x192 android-chrome-192x192.png
convert logo.svg.png -resize 512x512 android-chrome-512x512.png
convert logo.svg.png -resize 256x256 icon-256x256.png
svgo -i logo.svg -o logo.optim.svg
