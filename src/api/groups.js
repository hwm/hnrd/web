import { API_HOST, req } from './utils'

import groups from '../stores/groups'
import homeworks from '../stores/homeworks'

function groupResponse(resp) {
  groups.update(groups => ({ ...groups, [resp.group.id]: resp.group }))

  if (resp.homeworks) {
    homeworks.update(homeworks => {
      let result = { ...homeworks }

      // remove all previous homeworks from this group
      for (const [id, homework] of Object.entries(homeworks)) {
        if (homework.group_id == resp.group.id) {
          delete result[id]
        }
      }

      // add response's homeworks
      for (const homework of resp.homeworks) {
        result[homework.id] = homework
      }

      return result
    })
  }

  return resp
}

export function createGroup(groupData) {
  return req(`${API_HOST}/groups`, {
    method: 'POST',
    json: groupData,
  })
    .then(groupResponse)
    .then(({ group }) => group)
}

export function getGroup(id, secret) {
  return req(`${API_HOST}/groups/${id}?secret=${secret}`).then(groupResponse)
}

export function updateGroup(groupData, secret) {
  return req(`${API_HOST}/groups/${groupData.id}?secret=${secret}`, {
    method: 'POST',
    json: groupData,
  }).then(groupResponse)
}
