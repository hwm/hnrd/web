import { API_HOST, req } from './utils'

import homeworkResources from '../stores/homeworkResources'

function homeworkResourcesResponse(homeworkId, resources) {
  homeworkResources.update(_ => ({ ..._, [homeworkId]: resources }))
}

export function getHomeworkResources(homeworkId, auth) {
  return req(
    `${API_HOST}/homeworks/${homeworkId}/resources?secret=${auth.secret}`,
  ).then(({ resources }) => {
    homeworkResourcesResponse(homeworkId, resources)
    return resources
  })
}

export function uploadHomeworkResource({
  homeworkId,
  auth,
  onProgressFn = null,
  file,
}) {
  const formData = new FormData()
  formData.append('file', file)

  const xhr = new XMLHttpRequest()

  return [
    new Promise((resolve, reject) => {
      xhr.overrideMimeType('application/json')
      xhr.open(
        'POST',
        `${API_HOST}/homeworks/${homeworkId}/resources?secret=${auth.secret}`,
      )
      xhr.onload = function() {
        const json = JSON.parse(xhr.responseText)
        if (json.error) return reject(json.error)
        const { resource } = json
        homeworkResources.update(resources => ({
          ...resources,
          [homeworkId]: [...(resources[homeworkId] || []), resource],
        }))
        resolve(resource)
      }
      xhr.onerror = function() {
        reject(xhr)
      }
      xhr.upload.onprogress = function(event) {
        if (event.lengthComputable) {
          const progress = event.loaded / event.total
          if (onProgressFn) onProgressFn(progress)
        }
      }
      xhr.send(formData)
    }),
    () => xhr.abort(),
  ]
}

export function deleteHomeworkResource(resourceId, auth) {
  return req(
    `${API_HOST}/homework_resources/${resourceId}?secret=${auth.secret}`,
    { method: 'DELETE' },
  ).then(({ homework_id, resources }) => {
    homeworkResourcesResponse(homework_id, resources)
    return resources
  })
}
