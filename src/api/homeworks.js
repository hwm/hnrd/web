import { API_HOST, req } from './utils'

import homeworks from '../stores/homeworks'

function homeworkResponse(homework) {
  homeworks.update(homeworks => ({ ...homeworks, [homework.id]: homework }))
}

export function createHomework(homeworkData, auth) {
  return req(`${API_HOST}/groups/${auth.id}/homeworks?secret=${auth.secret}`, {
    method: 'POST',
    json: homeworkData,
  }).then(({ homework }) => {
    homeworkResponse(homework)
    return homework
  })
}

export function updateHomework(homeworkData, secret) {
  return req(`${API_HOST}/homeworks/${homeworkData.id}?secret=${secret}`, {
    method: 'POST',
    json: homeworkData,
  }).then(({ homework }) => {
    homeworkResponse(homework)
    return homework
  })
}
