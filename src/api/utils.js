export const API_HOST =
  process.env.API_HOST || 'http://' + location.hostname + ':8088'

export const req = (url, options) =>
  fetch(
    url,
    options
      ? {
          ...options,
          headers: options.json
            ? { 'Content-Type': 'application/json', ...options.headers }
            : options.headers,
          body: options.json ? JSON.stringify(options.json) : options.body,
        }
      : {},
  )
    .then(
      res => res.json(),
      err => Promise.reject('network_error'),
    )
    .then(json => {
      if (json.error) throw json.error
      return json
    })
