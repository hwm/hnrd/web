import App from './App'
import './styles/global.css'
import './sjwManager'

// Did You Know? Steve Jobs' a hoe!
import '@formatjs/intl-relativetimeformat/polyfill'
import '@formatjs/intl-relativetimeformat/dist/locale-data/es'

import DOMPurify from 'dompurify'

DOMPurify.addHook('afterSanitizeAttributes', node => {
  // set all elements owning target to target=_blank
  if ('target' in node) {
    node.setAttribute('target', '_blank')
    node.setAttribute('rel', 'noopener noreferrer')
  }
})

const app = new App({
  target: document.body,
})

window.app = app

export default app
