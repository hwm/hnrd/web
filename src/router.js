import { readable } from 'svelte/store'

export default function router(routes) {
  const calculateRouterRoute = () => calculateRoute(routes)
  const currentRoute = readable(calculateRouterRoute(), set => {
    const handler = event => set(calculateRouterRoute())
    window.addEventListener('hashchange', handler)

    return () => window.removeEventListener('hashchange', handler)
  })

  return currentRoute
}

export function back() {
  history.back()
}

function calculateRoute(routes) {
  if (document.location.hash.length < 3) document.location.hash = '#/'
  for (const route of routes) {
    const matches = document.location.hash.slice(1).match(route.regExp)
    if (matches) {
      console.log('chose route', route)
      if (route.redirect) {
        window.history.replaceState(null, '', `#${route.redirect}`)
        return calculateRoute(routes)
      }
      return { ...route, matches }
    }
  }
  throw new Error('No valid route found')
}
