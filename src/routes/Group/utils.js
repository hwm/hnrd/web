import getDate from '../../utils/getDate'

function categorizeHomeworks(homeworks) {
  let _archived = []
  let _categories = {}

  const a = (name, showDeadline) =>
    (_categories[name] = { homeworks: [], showDeadline })

  // Definir orden
  a('Tareas pasadas', true)
  a('Sin fecha definida', false)
  a('Para mañana', false)
  a('Para pasado mañana', false)
  a('Dentro de una semana', true)
  a('A futuro', true)

  const now = Date.now()

  for (const hw of homeworks) {
    const date = hw.deadline_date && getDate(hw.deadline_date)
    const s = name => _categories[name].homeworks.push(hw)

    if (hw.archived) _archived.push(hw)
    else if (date && date <= now) s('Tareas pasadas')
    else if (!date) s('Sin fecha definida')
    else if (date < now + 1000 * 60 * 60 * 24) s('Para mañana')
    else if (date < now + 1000 * 60 * 60 * 24 * 2) s('Para pasado mañana')
    else if (date < now + 1000 * 60 * 60 * 24 * 7) s('Dentro de una semana')
    else s('A futuro')
  }

  return [_archived, _categories]
}

export { categorizeHomeworks }
