const finalAssets = ['/', ...serviceWorkerOption.assets]

console.log('got assets:', serviceWorkerOption.assets)
console.log('final assets:', finalAssets)

const IMAGE_CACHE_NAME = 'hwm-homework-images'

self.addEventListener('install', e => {
  const CACHE_NAME = 'hwm-assets-' + new Date().toISOString()
  e.waitUntil(
    caches
      .keys()
      .then(names =>
        names
          .filter(name => name !== IMAGE_CACHE_NAME)
          .forEach(name => caches.delete(name)),
      )
      .then(() => caches.open(CACHE_NAME))
      .then(cache => cache.addAll(finalAssets)),
  )
})

self.addEventListener('fetch', event => {
  console.log('serving from sw:', event.request.url)

  if (event.request.method !== 'GET') return

  event.respondWith(
    caches.match(event.request).then(response => {
      if (response) console.log('serving from cache:', event.request.url)
      return response || fetch(event.request)
    }),
  )
})

self.addEventListener('message', messageEvent => {
  if (messageEvent.data === 'skipWaiting') return skipWaiting()
})
