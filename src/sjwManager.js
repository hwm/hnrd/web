import updateAvailable from './stores/updateAvailable'
import runtime from 'serviceworker-webpack-plugin/lib/runtime'

// https://gist.github.com/Klerith/80abd742d726dd587f4bd5d6a0ab26b6
function urlBase64ToUint8Array(base64String) {
  var padding = '='.repeat((4 - (base64String.length % 4)) % 4)
  var base64 = (base64String + padding).replace(/\-/g, '+').replace(/_/g, '/')

  var rawData = window.atob(base64)
  var outputArray = new Uint8Array(rawData.length)

  for (var i = 0; i < rawData.length; ++i) {
    outputArray[i] = rawData.charCodeAt(i)
  }
  return outputArray
}

let reg

async function registerServiceWorker() {
  if ('serviceWorker' in navigator) {
    reg = await runtime.register()
    console.log(reg)

    function listenForWaitingServiceWorker(reg, callback) {
      function awaitStateChange() {
        reg.installing.addEventListener('statechange', function() {
          if (this.state === 'installed') callback(reg)
        })
      }
      if (!reg) return
      if (reg.waiting && reg.active) return callback(reg)
      // si se está instalando una actualización
      if (reg.installing && reg.active) awaitStateChange()
      if (reg.active) reg.addEventListener('updatefound', awaitStateChange)
    }

    let refreshing
    navigator.serviceWorker.addEventListener('controllerchange', () => {
      if (refreshing) return
      refreshing = true
      window.location.reload()
    })

    function promptUserToRefresh(reg) {
      window.__registration = reg
      updateAvailable.set(true)
      console.log('prompted')
    }

    listenForWaitingServiceWorker(reg, promptUserToRefresh)
  }
}

process.env.NODE_ENV === 'production' && registerServiceWorker()
