import { writable } from 'svelte/store'
import localForage from 'localforage'

import { downloadImagesIntoCache } from '../utils/offlineUtils'
import getImageUrls from '../utils/getImageUrls'

const store = writable({})

store.subscribe(resources => {
  for (const resourcesArr of Object.values(resources)) {
    downloadImagesIntoCache(getImageUrls(resourcesArr))
  }
  localForage.setItem('homeworkResourcesStore', resources)
})

export async function setupStore() {
  const data = (await localForage.getItem('homeworkResourcesStore')) || {}
  store.update(() => data)
  return store
}

export default store
