import { writable } from 'svelte/store'
import localForage from 'localforage'

const store = writable({})

store.subscribe(groups => {
  localForage.setItem('homeworksStore', groups)
})

export async function setupStore() {
  const data = (await localForage.getItem('homeworksStore')) || {}
  store.update(() => data)
  return store
}

export default store
