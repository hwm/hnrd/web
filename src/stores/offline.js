import { readable } from 'svelte/store'

export default readable(!navigator.onLine, set => {
  const on = () => set(true)
  const off = () => set(false)
  window.addEventListener('online', off)
  window.addEventListener('offline', on)
  return () => {
    window.removeEventListener('online', off)
    window.removeEventListener('offline', on)
  }
})
