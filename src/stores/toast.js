import { writable } from 'svelte/store'

const store = writable(null)

let timeoutId

export { store }
export function setToast(message, buttons = [], timeout = 5000) {
  if (timeoutId) clearTimeout(timeoutId)
  timeoutId = setTimeout(() => store.set(null), timeout)

  return store.set({ message, buttons, timeout })
}
