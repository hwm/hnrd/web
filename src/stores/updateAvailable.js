import { writable } from 'svelte/store'

const store = writable(false)
export default store
