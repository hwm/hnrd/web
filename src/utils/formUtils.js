function submitWrapper(fn) {
  return event => {
    let values = {}
    Array.from(event.target.elements).forEach(el => {
      switch (el.nodeName) {
        case 'TEXTAREA':
        case 'INPUT':
          switch (el.type) {
            case 'checkbox':
              values[el.name] = el.checked
              break
            case 'radio':
              if (el.checked) values[el.name] = el.value
              break
            default:
              values[el.name] = el.value
          }
      }
    })
    fn(values)
  }
}

export { submitWrapper }
