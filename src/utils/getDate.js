export default function getDate(date) {
  return new Date(
    +new Date(date) - -(new Date().getTimezoneOffset() * 60 * 1000),
  )
}
