import localForage from 'localforage'

export default function getGroupAuth(id) {
  return localForage
    .getItem('groups')
    .then(groups => groups || [])
    .then(groups => groups.find(group => group.id == id))
    .then(auth => auth || Promise.reject('not_in_group'))
}
