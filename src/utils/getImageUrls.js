import { API_HOST } from '../api/utils'

export default function getImageUrls(resources) {
  return resources.map(
    ({ file_path }) => `${API_HOST}/image_uploads/${file_path}`,
  )
}
