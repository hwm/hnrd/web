const gradients = [
  'linear-gradient(140deg, #FC466B 0%, #3F5EFB 100%)',
  'linear-gradient(240deg, #3F2B96 0%, #A8C0FF 100%)',
  'linear-gradient( 90deg, #d53369 0%, #daae51 100%)',
]

export default function getGroupGradient(id) {
  return gradients[id % gradients.length]
}
