import localForage from 'localforage'
import { setToast } from '../stores/toast'
import groupsAuthRefreshKey from '../stores/groupsAuthRefreshKey'

export default async function leaveGroup(group, auth) {
  // Remove groups
  let groups = await localForage.getItem('groups')
  groups = groups.filter(_group => _group.id !== group.id)
  await localForage.setItem('groups', groups)

  // Toggle a refresh
  groupsAuthRefreshKey.update(n => ++n)

  // Redirect to groups
  location.hash = '#/groups'

  // Show toast
  setToast(
    group ? `Saliste del grupo ${group.name}` : `Saliste de un grupo.`,
    [
      {
        text: 'Deshacer',
        fn: async () => {
          let groups = await localForage.getItem('groups')
          groups.push(auth)
          await localForage.setItem('groups', groups)
          groupsAuthRefreshKey.update(n => ++n)
        },
      },
    ],
    10 * 1000,
  )
}
