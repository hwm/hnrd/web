import getGroupAuth from './getGroupAuth'
import getImageUrls from './getImageUrls'

import localForage from 'localforage'
import { get } from 'svelte/store'

import groups from '../stores/groups'
import homeworks from '../stores/homeworks'
import homeworkResources from '../stores/homeworkResources'

import { getGroup } from '../api/groups'
import { getHomeworkResources } from '../api/homeworkResources'

export async function downloadImagesIntoCache(urls) {
  const cache = await caches.open('hwm-homework-images')
  return await Promise.all(
    urls.map(url => cache.match(url).then(match => match || cache.add(url))),
  )
}

async function ensureGroupOffline(
  { id, secret },
  { $groups, $homeworks, $homeworkResources },
) {
  let group = $groups[id]
  // check downloaded group
  if (!group) group = (await getGroup(id, secret)).group

  const groupHomeworks = Object.values($homeworks).filter(
    ({ group_id }) => group_id === id,
  )
  for (const homework of groupHomeworks) {
    let resources = $homeworkResources[homework.id]
    if (!resources) {
      resources = await getHomeworkResources(homework.id, { id, secret })
    }

    // check downloaded resources metadata
    const urls = getImageUrls(resources)

    // check actual resources downloaded
    const cache = await caches.open('hwm-homework-images')
    const allResourcesDownloaded = (
      await Promise.all(urls.map(url => cache.match(url)))
    ).every(Boolean)

    if (!allResourcesDownloaded) return downloadImagesIntoCache(urls)
  }

  return true
}

export async function ensureOffline() {
  const auths = (await localForage.getItem('groups')) || []

  const resolvedStores = {
    $groups: get(groups),
    $homeworks: get(homeworks),
    $homeworkResources: get(homeworkResources),
  }

  const resolvedGroups = await Promise.all(
    auths.map(auth => ensureGroupOffline(auth, resolvedStores)),
  )
  return resolvedGroups.every(Boolean)
}
