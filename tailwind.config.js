module.exports = {
  theme: {
    extend: {
      screens: {
        dark: { raw: '(prefers-color-scheme: dark)' },
      },
      height: {
        '7': '1.75rem',
      },
      maxWidth: {
        lg: '400px',
      },
      minHeight: {
        md: '9rem',
        lg: '12rem',
        screen: '100vh',
      },
      borderRadius: {
        xl: '.75rem',
      },
      colors: {
        smoke: {
          darkest: 'rgba(0, 0, 0, .9)',
          darker: 'rgba(0, 0, 0, .75)',
          dark: 'rgba(0, 0, 0, .6)',
          default: 'rgba(0, 0, 0, .5)',
          light: 'rgba(0, 0, 0, .4)',
          lighter: 'rgba(0, 0, 0, .25)',
          lightest: 'rgba(0, 0, 0, .1)',
        },
      },
    },
  },
  variants: {},
  plugins: [],
  purge: {
    content: ['./**/*.svelte', './**/*.html'],
    options: {
      // See https://github.com/tailwindcss/tailwindcss/issues/1731 
      whitelistPatterns: [/svelte-/],
      defaultExtractor: content => {
        const regExp = new RegExp(/[A-Za-z0-9-_:/]+/g)
    
        const matchedTokens = []
    
        let match = regExp.exec(content)
        // To make sure that you do not lose any tailwind classes used in class directive.
        // https://github.com/tailwindcss/discuss/issues/254#issuecomment-517918397
        while (match) {
          if (match[0].startsWith('class:')) {
            matchedTokens.push(match[0].substring(6))
          } else {
            matchedTokens.push(match[0])
          }
    
          match = regExp.exec(content)
        }
    
        return matchedTokens
      },    
    },
  }
}
