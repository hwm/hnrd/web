const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const CopyPlugin = require('copy-webpack-plugin')
const ServiceWorkerWebpackPlugin = require('serviceworker-webpack-plugin')
const webpack = require('webpack')
const path = require('path')
const glob = require('glob-all')

const mode = process.env.NODE_ENV || 'development'
const prod = mode === 'production'

module.exports = {
  entry: {
    bundle: ['./src/main.js'],
  },
  resolve: {
    extensions: ['.mjs', '.js', '.svelte'],
    mainFields: ['svelte', 'browser', 'module', 'main'],
  },
  output: {
    path: __dirname + '/dist',
    filename: '[hash].js',
    chunkFilename: '[name].[id].[hash].js',
  },
  module: {
    rules: [
      {
        test: /\.(svelte)$/,
        use: {
          loader: 'svelte-loader',
          options: {
            emitCss: true,
            hotReload: true,
          },
        },
      },
      {
        test: /\.svg$/,
        use: ['svelte-loader', 'svelte-svg-loader'],
      },
      {
        test: /\.css$/,
        use: [
          /**
           * MiniCssExtractPlugin doesn't support HMR.
           * For developing, use 'style-loader' instead.
           * */
          prod ? MiniCssExtractPlugin.loader : 'style-loader',
          'css-loader',
          'postcss-loader',
        ],
      },
    ],
  },
  mode,
  plugins: [
    new MiniCssExtractPlugin({
      filename: '[hash].css',
    }),
    new HtmlWebpackPlugin({
      template: 'src/index.html',
      title: 'Manejador de Tareas',
      minify: {
        collapseWhitespace: true,
        minifyCSS: true,
        removeAttributeQuotes: true,
      },
    }),
    new CopyPlugin([{ from: 'public', to: '.' }]),
    new webpack.EnvironmentPlugin({
      API_HOST: null,
    }),
    new ServiceWorkerWebpackPlugin({
      entry: path.join(__dirname, 'src/sjw.js'),
    }),
  ],
  ...(prod
    ? {}
    : {
        cache: {
          type: 'filesystem',
        },
      }),
  devtool: prod ? 'source-map' : 'eval-source-map',
  devServer: {
    contentBase: path.join(__dirname, 'public'),
    host: '0.0.0.0',
    port: 8080,
  },
}
